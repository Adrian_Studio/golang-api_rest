package helper

import (
	"backend-golang/api/v1/middleware"
	"fmt"
	"math/rand"
	"time"
)

// LaunchProject simulates the launch of the project
func LaunchProject(id int64) {

	min := 0
	max := 30
	randomInt := rand.Intn(max-min) + min

	if randomInt < 5 {
		errorProjectProcess(id)
	} else {
		go func() {
			startProjectProcess(id, randomInt)
		}()
	}
	// fmt.Println(randomInteger)
}

func startProjectProcess(id int64, processTime int) {
	fmt.Println("SIMULATES PROCESSING")
	// get the db connection
	db := middleware.GetConnection()

	// create the update sql query
	sqlStatement := `UPDATE project SET initProcessDate=now(), state="PROCESSING" WHERE id=?`
	response, err := db.Exec(sqlStatement, id)

	if err != nil {
		fmt.Println("Unable to execute the query. ERROR: ", err)
	}
	rowsAffected, _ := response.RowsAffected()
	if rowsAffected == 0 {
		fmt.Println("No rows affected")
	}

	time.Sleep(time.Minute * time.Duration(rand.Int31n(int32(processTime))))

	// create the update sql query
	sqlStatement = `UPDATE project SET endProcessDate=now(), state="PROCESSED" WHERE id=?`
	_, err = db.Exec(sqlStatement, id)

	if err != nil {
		fmt.Println("Unable to execute the query. ERROR: ", err)
	}

	rowsAffected, _ = response.RowsAffected()
	if rowsAffected == 0 {
		fmt.Println("No rows affected")
	}
	fmt.Println("SIMULATION ENDED")
}

func errorProjectProcess(id int64) error {
	fmt.Println("SIMULATES ERROR")
	// get the db connection
	db := middleware.GetConnection()

	// create the update sql query
	sqlStatement := `UPDATE project SET initProcessDate=now(), state="ERROR" WHERE id=?`
	_, err := db.Exec(sqlStatement, id)

	if err != nil {
		fmt.Println("Unable to execute the query. ERROR: ", err)
		return err
	}

	// return empty user on error
	return err
}
