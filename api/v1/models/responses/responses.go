package modelsResponses

// User schema of the user table
type ResponseCreate struct {
	ID      int64  `json:"id,omitempty"`
	Message string `json:"message,omitempty"`
}

// User schema of the user table
type ResponseUpdate struct {
	ID      int64  `json:"id,omitempty"`
	Message string `json:"message,omitempty"`
}

// User schema of the user table
type ResponseDelete struct {
	ID      int64  `json:"id,omitempty"`
	Message string `json:"message,omitempty"`
}

// User schema of the user table
type ResponseRelauch struct {
	ID      int64  `json:"id,omitempty"`
	Message string `json:"message,omitempty"`
}
