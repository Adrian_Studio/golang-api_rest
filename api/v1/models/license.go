package models

type LicenseResponse struct {
	ID           int64  `json:"id"`
	Name         string `json:"license"`
	Type         string `json:"type"`
	Url          string `json:"url"`
	Annotations  string `json:"annotations"`
	CreationDate string `json:"creation_date"`
}

type License struct {
	ID          int64  `json:"id"`
	Name        string `json:"license"`
	Type        string `json:"type"`
	Url         string `json:"url"`
	Annotations string `json:"annotations"`
}

type GetLicensesFilter struct {
	ShowWithoutType bool   `json:"showWithoutType"`
	FilterType      string `json:"filterType"`
	FilterSearch    string `json:"filterSearch"`
}
