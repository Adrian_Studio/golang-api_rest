package models

import (
	"time"
)

type Project struct {
	ID                  int64     `json:"id"`
	IDProject           string    `json:"idproject"`
	Name                string    `json:"name"`
	Description         string    `json:"description"`
	UniqueFile          int       `json:"uniqueFile"`
	ExcludeEverisLib    int       `json:"excludeEverisLib"`
	State               string    `json:"state"`
	CreatonDate         time.Time `json:"creationDate"`
	InitProcessDate     time.Time `json:"initProcessDate"`
	EndProcessDate      time.Time `json:"endProcessDate"`
	User                string    `json:"user"`
	Info                string    `json:"info"`
	Url                 string    `json:"url"`
	Annotations         string    `json:"annotations"`
	Licenses            int       `json:"licenses"`
	LicensesWithoutType int       `json:"licensesWithoutType"`
}
