// main_test.go

package handler

import (
	"backend-golang/api/v1/handler"
	modelsResponses "backend-golang/api/v1/models/responses"
	routerTest "backend-golang/api/v1/router"
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	"github.com/gorilla/mux"
)

var testId int64

func executeRequest(r *http.Request, f func(http.ResponseWriter, *http.Request)) *httptest.ResponseRecorder {
	rec := httptest.NewRecorder()
	routerTest.Router()

	handler := http.HandlerFunc(f)
	handler.ServeHTTP(rec, r)
	return rec
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

type HandlerTest struct {
	params       string
	queryParams  string
	body         string
	expectedCode int
}

func TestGetLicensesFiltered(t *testing.T) {
	var handlerGetLicensesFilteredTests = []HandlerTest{
		{
			body:         `{"showWithoutType": false}`,
			expectedCode: 200,
		},
		{
			body:         `{"showWithoutType": true}`,
			expectedCode: 200,
		},
		{
			body:         `{"showWithoutType": false, "filterType": "name" }`,
			expectedCode: 200,
		},
		{
			body:         `{"showWithoutType": false, "filterType": "name", "filterSearch": "licencia pru"}`,
			expectedCode: 200,
		},
		{
			body:         `{"showWithoutType": false, "filterType": "type", "filterSearch": "licencia pru"}`,
			expectedCode: 200,
		},
	}
	for _, test := range handlerGetLicensesFilteredTests {
		var body = []byte(test.body)

		req, _ := http.NewRequest(http.MethodPost, "/api/v1/license/filtered", bytes.NewBuffer(body))

		response := executeRequest(req, handler.GetLicensesFiltered)

		checkResponseCode(t, test.expectedCode, response.Code)
		// if body := response.Body.String(); body.String() != test.expectedBody {
		// 	t.Errorf("Expected an empty array. Got %s", body)
		// }
	}
}

func TestGetIfNameExist(t *testing.T) {
	var handlerGetIfNameExistsTests = []HandlerTest{
		{
			queryParams:  "name=MIT",
			expectedCode: 200,
		},
		{
			queryParams:  "name=MI",
			expectedCode: 200,
		},
	}
	for _, test := range handlerGetIfNameExistsTests {

		req, _ := http.NewRequest(http.MethodGet, "/api/v1/license/namecheck", nil)
		req.URL.RawQuery = test.queryParams

		response := executeRequest(req, handler.GetIfNameExist)

		checkResponseCode(t, test.expectedCode, response.Code)
		// if body := response.Body.String(); body.String() != test.expectedBody {
		// 	t.Errorf("Expected an empty array. Got %s", body)
		// }
	}
}

func TestCreateLicense(t *testing.T) {
	var handlerCreateTests = []HandlerTest{
		{
			body: `{
				"license": "licencia pruebas 2",
				"type": "tipo 1",
				"url": "http...",
				"annotations": null
				}`,
			expectedCode: 201,
		},
		{
			body: `{
				"license": "licencia pruebas 2"
				}`,
			expectedCode: 201,
		},
		// {
		// 	body: `{
		// 		}`,
		// 	expectedCode: 400,
		// },
	}
	for _, test := range handlerCreateTests {
		var body = []byte(test.body)

		req, _ := http.NewRequest(http.MethodPost, "/api/v1/license/", bytes.NewBuffer(body))

		response := executeRequest(req, handler.CreateLicense)
		var formatedRes modelsResponses.ResponseCreate
		err := json.Unmarshal([]byte(response.Body.Bytes()), &formatedRes)
		if err != nil {
			t.Errorf("Error. %v ", err)
		}
		if response.Code == 201 {
			testId = formatedRes.ID
		}
		checkResponseCode(t, test.expectedCode, response.Code)

		// if body := response.Body.String(); body.String() != test.expectedBody {
		// 	t.Errorf("Expected an empty array. Got %s", body)
		// }
	}
}

func TestGetLicense(t *testing.T) {
	var handlerGetLicenseTests = []HandlerTest{
		{
			params:       `{"id": "` + strconv.FormatInt(testId, 10) + `"}`,
			expectedCode: 200,
		},
		{
			params:       `{"id": "` + strconv.FormatInt(testId+1, 10) + `"}`,
			expectedCode: 404,
		},
	}

	for _, test := range handlerGetLicenseTests {
		var params map[string]string
		err := json.Unmarshal([]byte(test.params), &params)
		if err != nil {
			t.Errorf("Error. %v ", err)
		}
		req, _ := http.NewRequest(http.MethodGet, "/api/v1/license", nil)
		req = mux.SetURLVars(req, params)

		response := executeRequest(req, handler.GetLicense)

		checkResponseCode(t, test.expectedCode, response.Code)
		// if body := response.Body.String(); body.String() != test.expectedBody {
		// 	t.Errorf("Expected an empty array. Got %s", body)
		// }
	}
}

func TestUpdateLicense(t *testing.T) {
	var handlerGetLicenseTests = []HandlerTest{
		{
			params:       `{"id": "` + strconv.FormatInt(testId, 10) + `"}`,
			expectedCode: 200,
			body: `{
				"license": "licencia updated",
				"type": "tipo 1",
				"url": "http...",
				"annotations": null
				}`,
		},
		{
			params:       `{"id": "` + strconv.FormatInt(testId+1, 10) + `"}`,
			expectedCode: 404,
			body: `{
				"license": "licencia updated",
				"type": "tipo 1",
				"url": "http...",
				"annotations": null
				}`,
		},
	}

	for _, test := range handlerGetLicenseTests {
		var body = []byte(test.body)
		var params map[string]string
		err := json.Unmarshal([]byte(test.params), &params)
		if err != nil {
			t.Errorf("Error. %v ", err)
		}

		req, _ := http.NewRequest(http.MethodPut, "/api/v1/license/", bytes.NewBuffer(body))
		req = mux.SetURLVars(req, params)

		response := executeRequest(req, handler.UpdateLicense)

		checkResponseCode(t, test.expectedCode, response.Code)
		// if body := response.Body.String(); body.String() != test.expectedBody {
		// 	t.Errorf("Expected an empty array. Got %s", body)
		// }
	}
}

func TestDeleteLicense(t *testing.T) {
	var handlerGetLicenseTests = []HandlerTest{
		{
			params:       `{"id": "` + strconv.FormatInt(testId, 10) + `"}`,
			expectedCode: 200,
		},
		{
			params:       `{"id": "` + strconv.FormatInt(testId+1, 10) + `"}`,
			expectedCode: 404,
		},
	}

	for _, test := range handlerGetLicenseTests {
		var params map[string]string
		err := json.Unmarshal([]byte(test.params), &params)
		if err != nil {
			t.Errorf("Error. %v ", err)
		}

		req, _ := http.NewRequest(http.MethodDelete, "/api/v1/license/", nil)
		req = mux.SetURLVars(req, params)

		response := executeRequest(req, handler.DeleteLicense)

		checkResponseCode(t, test.expectedCode, response.Code)
		// if body := response.Body.String(); body.String() != test.expectedBody {
		// 	t.Errorf("Expected an empty array. Got %s", body)
		// }
	}
}
