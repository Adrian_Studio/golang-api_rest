package middleware

import (
	"database/sql" // package to encode and decode the json into struct and vice versa
	"fmt"          // used to access the request and response object of the api
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"

	// used to read the environment variable
	_ "github.com/go-sql-driver/mysql" // postgres golang driver
	"github.com/joho/godotenv"         // package used to read the .env file
)

var (
	once sync.Once
	db   *sql.DB
)

// get connection with postgres db
func GetConnection() *sql.DB {
	once.Do(func() {

		f, _ := os.Getwd()
		absPath, _ := filepath.Abs(f)
		indexCut := strings.Index(absPath, "api\\v")
		projectPath := absPath
		if indexCut != -1 {
			projectPath = absPath[:indexCut]
		}

		env := fmt.Sprintf("%s/internal/.env", projectPath)

		// load .env file
		err := godotenv.Load(fmt.Sprint(env))

		if err != nil {
			log.Fatalf("Error loading .env file. ERROR: %v", err)
		}

		mysqlInfo := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true",
			os.Getenv("DB_USERNAME"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_NAME"))

		// Open the connection
		db, err = sql.Open("mysql", mysqlInfo)

		if err != nil {
			log.Fatalf("Error conecting to the db. ERROR: %v", err)
		}

		// // solo habrá 24 conexiones abiertas
		db.SetMaxOpenConns(24)

		// // MaxIdleConns nunca puede ser inferior al
		// // máximo de SetMaxOpenConns abiertas de lo contrario,
		// // tendrá ese valor predeterminado
		// db.SetMaxIdleConns(24)

		// check the connection
		err2 := db.Ping()

		if err2 != nil {
			log.Fatalf("Error conecting to the db. ERROR: %v", err2)
			panic(err2)
		}

		fmt.Println("Successfully connected to the db!")

	})

	// return the connection
	return db
}
