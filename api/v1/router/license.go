package router

import (
	"backend-golang/api/v1/handler"

	"github.com/gorilla/mux"
)

// Router is exported and used in router.go
func LicenseRouter(router *mux.Router) {

	router.HandleFunc("/filtered", handler.GetLicensesFiltered).Methods("POST", "OPTIONS")
	router.HandleFunc("/namecheck", handler.GetIfNameExist).Methods("GET", "OPTIONS")
	router.HandleFunc("", handler.CreateLicense).Methods("POST", "OPTIONS")
	router.HandleFunc("/{id}", handler.GetLicense).Methods("GET", "OPTIONS")
	router.HandleFunc("/{id}", handler.UpdateLicense).Methods("PUT", "OPTIONS")
	router.HandleFunc("/{id}", handler.DeleteLicense).Methods("DELETE", "OPTIONS")

}
