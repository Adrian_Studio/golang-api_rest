// License checker API REST.
//
// API REST para el control de la base de datos del license checker
//
//	Schemes: https
//	Host: localhost:8080
//	BasePath: /api/v1/
//	Version: 1.0
//	License: MIT http://opensource.org/licenses/MIT
//
//
//	Consumes:
//	- application/json
//
//	Produces:
//	- application/json
//
// swagger:meta
package router

import (
	"backend-golang/api/v1/middleware"
	"net/http"

	openapimiddleware "github.com/go-openapi/runtime/middleware"
	"github.com/gorilla/mux"
)

// Router is exported and used in main.go
func Router() *mux.Router {
	version := "/v1"
	router := mux.NewRouter()
	// Define content type to json/aplication for all responses
	router.Use(middleware.DefaultResponseHeader)
	// Define cors
	router.Use(middleware.Cors)

	v1 := router.PathPrefix("/api" + version).Subrouter()
	// Project Router
	project := v1.PathPrefix("/project").Subrouter()
	ProjectRouter(project)

	// License Router
	license := v1.PathPrefix("/license").Subrouter()
	LicenseRouter(license)

	// handler for documentation
	opts := openapimiddleware.RedocOpts{Path: "/api" + version + "/docs", SpecURL: "/api" + version + "/swagger.yaml"}
	sh := openapimiddleware.Redoc(opts, nil)

	v1.Handle("/docs", sh)
	v1.Handle("/swagger.yaml", http.FileServer(http.Dir("../../docs/swagger/")))

	// Test database conection
	// get the db connection
	middleware.GetConnection()

	return router
}
