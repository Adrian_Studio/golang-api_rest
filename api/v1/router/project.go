package router

import (
	"backend-golang/api/v1/handler"

	"github.com/gorilla/mux"
)

// Router is exported and used in router.go
func ProjectRouter(router *mux.Router) {

	router.HandleFunc("/active", handler.GetActiveProject).Methods("GET", "OPTIONS")
	router.HandleFunc("/archived", handler.GetArchivedProject).Methods("GET", "OPTIONS")
	router.HandleFunc("", handler.CreateProject).Methods("POST", "OPTIONS")
	router.HandleFunc("/{id}", handler.GetProject).Methods("GET", "OPTIONS")
	router.HandleFunc("/{id}", handler.UpdateProject).Methods("PUT", "OPTIONS")
	router.HandleFunc("/file/{id}", handler.GetFile).Methods("GET", "OPTIONS")
	router.HandleFunc("/{id}", handler.DeleteProject).Methods("DELETE", "OPTIONS")
	router.HandleFunc("/relaunch/{id}", handler.RelaunchProject).Methods("POST", "OPTIONS")
	router.HandleFunc("/activate/{id}", handler.ActivateProject).Methods("POST", "OPTIONS")
	router.HandleFunc("/archivate/{id}", handler.ArchivateProject).Methods("POST", "OPTIONS")

}
