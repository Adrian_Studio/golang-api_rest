package handlerModels

import (
	"backend-golang/api/v1/middleware"
	"backend-golang/api/v1/models"
	"database/sql"
	"errors"
	"fmt"

	_ "github.com/go-sql-driver/mysql" // postgres golang driver
)

func GetLicensesFiltered(filters models.GetLicensesFilter) ([]models.LicenseResponse, error) {
	// get the db connection
	db := middleware.GetConnection()

	var licenses []models.LicenseResponse

	// create the select sql query
	sqlStatement := `SELECT id, license, 
					coalesce(type,"") as type, 
					coalesce(url,"") as url, 
					coalesce(annotations,"") as annotations, 
					creationDate FROM licenseType WHERE TRUE = TRUE `

	if !filters.ShowWithoutType {
		sqlStatement += ` AND type IS NOT NULL `
	}

	if filters.FilterType == "type" {
		sqlStatement += fmt.Sprintf(" AND type LIKE '%%%s%%' ", filters.FilterSearch)
	}

	if filters.FilterType == "name" {
		sqlStatement += fmt.Sprintf(" AND license LIKE '%%%s%%' ", filters.FilterSearch)
	}

	// execute the sql statement
	rows, err := db.Query(sqlStatement)

	if err != nil {
		fmt.Println("Unable to execute the query. ", err)
		return licenses, err
	}

	// close the statement
	defer rows.Close()

	// iterate over the rows
	for rows.Next() {
		var license models.LicenseResponse

		// unmarshal the row object to user
		err = rows.Scan(&license.ID, &license.Name, &license.Type, &license.Url, &license.Annotations, &license.CreationDate)

		if err != nil {
			fmt.Println("Unable to scan the row. ", err)
		}

		// append the user in the users slice
		licenses = append(licenses, license)

	}

	// return empty user on error
	return licenses, err
}

func GetIfNameExist(name string) (bool, error) {
	// get the db connection
	db := middleware.GetConnection()

	// create the select sql query
	sqlStatement := `SELECT license FROM licenseType `
	sqlStatement += fmt.Sprintf(" WHERE license LIKE '%s' ", name)

	err := db.QueryRow(sqlStatement).Scan(&name)

	if err != nil {
		if err != sql.ErrNoRows {
			// a real error happened! you should change your function return
			// to "(bool, error)" and return "false, err" here
			fmt.Println("Sql error happened. ERROR: ", err)
			return false, err
		}

		return false, nil
	}

	// return empty user on error
	return true, err
}

func CreateLicense(license models.License) (int64, error) {
	// get the db connection
	db := middleware.GetConnection()

	// create the insert sql query
	sqlStatement := `INSERT INTO licenseType (license, type, url, annotations) VALUES (?, ?, ?, ?);`

	response, err := db.Exec(sqlStatement, license.Name, license.Type, license.Url, license.Annotations)

	if err != nil {
		fmt.Println("Unable to execute the query. ERROR: ", err)
		return -1, err
	}

	lastId, err := response.LastInsertId()

	if err != nil {
		fmt.Println("Unable to execute the query. ERROR: ", err)
		return lastId, err
	}

	// return empty user on error
	return lastId, err
}

func GetLicense(id int) (models.LicenseResponse, error) {
	// get the db connection
	db := middleware.GetConnection()

	var license models.LicenseResponse

	// create the select sql query
	sqlStatement := `SELECT id, license, 
	coalesce(type,"") as type, 
	coalesce(url,"") as url, 
	coalesce(annotations,"") as annotations, 
	creationDate FROM licenseType WHERE id = ? `

	err := db.QueryRow(sqlStatement, id).Scan(&license.ID, &license.Name, &license.Type, &license.Url, &license.Annotations, &license.CreationDate)

	if err != nil {
		fmt.Println("Unable to execute the query. ", err)
	}
	// return empty user on error
	return license, err
}

func UpdateLicense(id int64, license models.License) (int64, error) {
	// get the db connection
	db := middleware.GetConnection()

	// create the update sql query
	sqlStatement := `UPDATE licenseType SET license=?, type=?, url=?, annotations=? WHERE id=?`

	_, err := db.Exec(sqlStatement, license.Name, license.Type, license.Url, license.Annotations, id)

	if err != nil {
		fmt.Println("Unable to execute the query. ERROR: ", err)
		return id, err
	}

	// rowsAffected, err := response.RowsAffected()
	// if rowsAffected == 0 {
	// 	fmt.Println("No rows affected")
	// 	return id, errors.New("id not found")
	// }

	// return empty user on error
	return id, err
}

func DeleteLicense(id int64) (int64, error) {
	// get the db connection
	db := middleware.GetConnection()

	// create the delete sql query
	sqlStatement := `DELETE FROM licenseType WHERE id=?`

	response, err := db.Exec(sqlStatement, id)

	if err != nil {
		fmt.Println("Unable to execute the query. ERROR: ", err)
		return id, err
	}

	rowsAffected, err := response.RowsAffected()
	if rowsAffected == 0 {
		fmt.Println("No rows affected")
		return id, errors.New("id not found")
	}

	// return empty user on error
	return id, err
}
