package handlerModels

import (
	"backend-golang/api/v1/middleware"
	"backend-golang/api/v1/models"
	"errors"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql" // postgres golang driver
)

func GetActiveProjects(size int) ([]models.Project, error) {
	// create the postgres db connection
	db := middleware.GetConnection()

	var projects []models.Project

	// create the select sql query
	sqlStatement := fmt.Sprintf(`SELECT p.id, p.idProject, name, 
	description, 
	uniqueFile, 
	excludeEverisLib, 
	state, 
	creationDate, 
	initProcessDate, 
	endProcessDate, 
	coalesce(user,"") as user, 
	coalesce(info,"") as info, 
	coalesce(url,"") as url,
	coalesce(annotations,"") as annotations,
	coalesce(sum(ps.total),0) as licenses,
	coalesce(sum(ps.licenseTypeMissing),0) as licensesWithoutType
	FROM project p
	left join projectstatistic ps on ps.idProject = p.id
	WHERE active = true 
	group by p.id
	LIMIT %d`, size)

	// execute the sql statement
	rows, err := db.Query(sqlStatement)

	if err != nil {
		log.Fatalf("Unable to execute the query. %v", err)
	}

	// close the statement
	defer rows.Close()

	// iterate over the rows
	for rows.Next() {
		var project models.Project

		// unmarshal the row object to user
		err = rows.Scan(&project.ID, &project.IDProject, &project.Name,
			&project.Description, &project.UniqueFile, &project.ExcludeEverisLib,
			&project.State, &project.CreatonDate, &project.InitProcessDate,
			&project.EndProcessDate, &project.User, &project.Info, &project.Url,
			&project.Annotations, &project.Licenses, &project.LicensesWithoutType)

		if err != nil {
			log.Fatalf("Unable to scan the row. %v", err)
		}
		// if project.Description.Valid {
		// 	use project.Description.String
		//  } else {
		// 	// NULL value
		//  }

		// append the user in the users slice
		projects = append(projects, project)

	}

	// return empty user on error
	return projects, err
}

func GetArchivedProjects(size int) ([]models.Project, error) {
	// create the postgres db connection
	db := middleware.GetConnection()

	var projects []models.Project

	// create the select sql query
	sqlStatement := fmt.Sprintf(`SELECT p.id, p.idProject, name, 
	description, 
	uniqueFile, 
	excludeEverisLib, 
	state, 
	creationDate, 
	initProcessDate, 
	endProcessDate, 
	coalesce(user,"") as user, 
	coalesce(info,"") as info, 
	coalesce(url,"") as url,
	coalesce(annotations,"") as annotations,
	coalesce(sum(ps.total),0) as licenses,
	coalesce(sum(ps.licenseTypeMissing),0) as licensesWithoutType
	FROM project p
	left join projectstatistic ps on ps.idProject = p.id
	WHERE active = false 
	group by p.id
	LIMIT %d`, size)

	// execute the sql statement
	rows, err := db.Query(sqlStatement)

	if err != nil {
		log.Fatalf("Unable to execute the query. %v", err)
	}

	// close the statement
	defer rows.Close()

	// iterate over the rows
	for rows.Next() {
		var project models.Project

		// unmarshal the row object to user
		err = rows.Scan(&project.ID, &project.IDProject, &project.Name,
			&project.Description, &project.UniqueFile, &project.ExcludeEverisLib,
			&project.State, &project.CreatonDate, &project.InitProcessDate,
			&project.EndProcessDate, &project.User, &project.Info, &project.Url,
			&project.Annotations, &project.Licenses, &project.LicensesWithoutType)

		if err != nil {
			log.Fatalf("Unable to scan the row. %v", err)
		}
		// if project.Description.Valid {
		// 	use project.Description.String
		//  } else {
		// 	// NULL value
		//  }

		// append the user in the users slice
		projects = append(projects, project)

	}

	// return empty user on error
	return projects, err
}

func CreateProject(project models.Project) (int64, error) {
	// get the db connection
	db := middleware.GetConnection()

	// create the insert sql query
	sqlStatement := `INSERT INTO project (idProject, name, description, uniqueFile, excludeEverisLib, annotations, url) VALUES (?, ?, ?, ?, ?, ?, ?);`

	response, err := db.Exec(sqlStatement, project.IDProject, project.Name, project.Description, project.UniqueFile, project.ExcludeEverisLib, project.Annotations, project.Url)

	if err != nil {
		fmt.Println("Unable to execute the query. ERROR: ", err)
		return -1, err
	}

	lastId, err := response.LastInsertId()

	if err != nil {
		fmt.Println("Unable to execute the query. ERROR: ", err)
		return lastId, err
	}

	// return empty user on error
	return lastId, err
}

func GetProject(id int) (models.Project, error) {
	// get the db connection
	db := middleware.GetConnection()

	var project models.Project

	// create the select sql query
	sqlStatement := `SELECT id, idProject, name, 
	description, 
	uniqueFile, 
	excludeEverisLib, 
	state, 
	creationDate, 
	initProcessDate, 
	endProcessDate, 
	coalesce(user,"") as user, 
	coalesce(info,"") as info, 
	coalesce(url,"") as url,
	coalesce(annotations,"") as annotations
	FROM project WHERE id = ? `

	err := db.QueryRow(sqlStatement, id).Scan(&project.ID, &project.IDProject, &project.Name,
		&project.Description, &project.UniqueFile, &project.ExcludeEverisLib,
		&project.State, &project.CreatonDate, &project.InitProcessDate,
		&project.EndProcessDate, &project.User, &project.Info, &project.Url,
		&project.Annotations)

	if err != nil {
		fmt.Println("Unable to execute the query. ", err)
	}
	// return empty project on error
	return project, err
}

func UpdateProject(id int64, project models.Project) (int64, string, error) {
	// get the db connection
	db := middleware.GetConnection()

	var err error
	var previousFile string

	if project.Url == "" {
		// create the update sql query
		sqlStatement := `UPDATE project SET name=?, description=?, uniqueFile=?, excludeEverisLib=?, annotations=? WHERE id=?`
		_, err = db.Exec(sqlStatement, project.Name, project.Description, project.UniqueFile, project.ExcludeEverisLib, project.Annotations, id)
	} else {
		sqlStatement := `SELECT url FROM project WHERE id=?`
		err = db.QueryRow(sqlStatement, id).Scan(&previousFile)
		if err != nil {
			fmt.Println("Unable to execute the query. ERROR: ", err)
			return id, "", err
		}
		// create the update sql query
		sqlStatement = `UPDATE project SET name=?, description=?, uniqueFile=?, excludeEverisLib=?, annotations=?, url=? WHERE id=?`

		_, err = db.Exec(sqlStatement, project.Name, project.Description, project.UniqueFile, project.ExcludeEverisLib, project.Annotations, project.Url, id)
	}

	if err != nil {
		fmt.Println("Unable to execute the query. ERROR: ", err)
		return id, "", err
	}

	// return empty user on error
	return id, previousFile, err
}

func DeleteProject(id int64) (int64, string, error) {
	// get the db connection
	db := middleware.GetConnection()

	var file string
	sqlStatement := `SELECT url FROM project WHERE id=?`

	err := db.QueryRow(sqlStatement, id).Scan(&file)
	if err != nil {
		fmt.Println("Unable to execute the query. ERROR: ", err)
		return id, "", err
	}

	// create the delete sql query
	sqlStatement = `DELETE FROM project WHERE id=?`

	response, err2 := db.Exec(sqlStatement, id)

	if err2 != nil {
		fmt.Println("Unable to execute the query. ERROR: ", err2)
		return id, "", err
	}

	rowsAffected, _ := response.RowsAffected()
	if rowsAffected == 0 {
		fmt.Println("No rows affected")
		return id, "", errors.New("id not found")
	}

	// return empty user on error
	return id, file, err
}

func GetProjectFile(id int64) (string, error) {
	// get the db connection
	db := middleware.GetConnection()

	var file string
	sqlStatement := `SELECT url FROM project WHERE id=?`

	err := db.QueryRow(sqlStatement, id).Scan(&file)
	if err != nil {
		fmt.Println("Unable to execute the query. ERROR: ", err)
		return "", err
	}

	// return empty user on error
	return file, err
}

func ActivateProject(id int64) (int64, error) {
	// get the db connection
	db := middleware.GetConnection()

	// create the update sql query
	sqlStatement := `UPDATE project SET active=true WHERE id=?`
	_, err := db.Exec(sqlStatement, id)

	if err != nil {
		fmt.Println("Unable to execute the query. ERROR: ", err)
		return id, err
	}

	// return empty user on error
	return id, err
}

func ArchivateProject(id int64) (int64, error) {
	// get the db connection
	db := middleware.GetConnection()

	// create the update sql query
	sqlStatement := `UPDATE project SET active=false WHERE id=?`
	_, err := db.Exec(sqlStatement, id)

	if err != nil {
		fmt.Println("Unable to execute the query. ERROR: ", err)
		return id, err
	}

	// return empty user on error
	return id, err
}
