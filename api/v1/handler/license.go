package handler

import (
	handlerModels "backend-golang/api/v1/handler/models" // models package where routes 1 schema is defined
	"backend-golang/api/v1/models"
	modelsResponses "backend-golang/api/v1/models/responses"
	"encoding/json" // package to encode and decode the json into struct and vice versa
	"errors"
	"fmt"
	"net/http" // used to access the request and response object of the api
	"strconv"

	_ "github.com/go-sql-driver/mysql" // postgres golang driver
	"github.com/gorilla/mux"
)

// swagger:route GET /license/filtered license list
// Returns a list of licenses
//     Parameters:
//       + name: limit
//         in: body
//         description: maximum numnber of results to return
//         required: false
//         type: integer
//         format: int32
// 		responses:
// 			200: productsResponse

// GetLicenseFiltered will return all the active projects
func GetLicensesFiltered(w http.ResponseWriter, r *http.Request) {

	var filters models.GetLicensesFilter

	// decode the json request to filters
	err := json.NewDecoder(r.Body).Decode(&filters)

	if err != nil {
		fmt.Println("Unable to decode the request body. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("Unable to decode the request body. ERROR: %v", err)))
		return
	}

	// get all the filtered licenses in the db
	licenses, err2 := handlerModels.GetLicensesFiltered(filters)

	if err2 != nil {
		w.WriteHeader(404)
		w.Write([]byte("Not Found"))
		return
	}

	// send all the filtered licenses as response
	json.NewEncoder(w).Encode(licenses)
}

// GetIfNameExist a boolean with the check if the name exists
func GetIfNameExist(w http.ResponseWriter, r *http.Request) {

	name := r.URL.Query().Get("name")

	// return a boolean with the check if the name exists
	exists, err := handlerModels.GetIfNameExist(name)

	if err != nil {
		fmt.Println("Unable to check if the name exists. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("Unable to check if the name exists. ERROR: %v", err)))
		return
	}

	// send all the projects as response
	json.NewEncoder(w).Encode(exists)
}

// CreateLicense will add a license to the database
func CreateLicense(w http.ResponseWriter, r *http.Request) {

	var license models.License

	// decode the json request to filters
	err := json.NewDecoder(r.Body).Decode(&license)

	if err != nil {
		fmt.Println("Unable to decode the request body. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("Unable to decode the request body. ERROR: %v", err)))
		return
	}

	if license.Name == "" {
		err = errors.New("the licence name can't be ''")
		fmt.Println("Unable to create. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("Unable to create. ERROR: %v", err)))
		return

	}

	// add the license to the database
	insertID, err2 := handlerModels.CreateLicense(license)

	if err2 != nil {
		fmt.Println("Unable to create. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("Unable to create. ERROR: %v", err)))
		return
	}
	w.WriteHeader(201)
	// format a response object
	res := modelsResponses.ResponseCreate{
		ID:      insertID,
		Message: "License created successfully",
	}

	// send the response
	json.NewEncoder(w).Encode(res)
}

// GetLicense will return a specific license
func GetLicense(w http.ResponseWriter, r *http.Request) {

	// get the id from the request params, key is "id"
	params := mux.Vars(r)

	// convert the id type from string to int
	id, err := strconv.Atoi(params["id"])

	if err != nil {
		fmt.Println("ID could not be converted to integer. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("ID could not be converted to integer. ERROR: %v", err)))
		return
	}

	// get the license form the database
	license, err2 := handlerModels.GetLicense(id)

	if err2 != nil {
		// there was an error
		fmt.Println("Unable to get license. ", err)
		w.WriteHeader(404)
		w.Write([]byte("Not Found"))
		return
	}

	// send the response
	json.NewEncoder(w).Encode(license)
}

// UpdateLicense will update a specific license
func UpdateLicense(w http.ResponseWriter, r *http.Request) {

	// get the id from the request params, key is "id"
	params := mux.Vars(r)

	var id int64
	// convert the id type from string to int
	idTemp, err := strconv.Atoi(params["id"])
	id = int64(idTemp)

	if err != nil {
		fmt.Println("ID could not be converted to integer. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("ID could not be converted to integer. ERROR: %v", err)))
		return
	}

	var license models.License

	// decode the json request to filters
	err2 := json.NewDecoder(r.Body).Decode(&license)

	if err2 != nil {
		fmt.Println("Unable to decode the request body. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("Unable to decode the request body. ERROR: %v", err)))
		return
	}

	// update the license form the database
	updatedID, err3 := handlerModels.UpdateLicense(id, license)

	if err3 != nil {
		fmt.Println("Unable to update. ERROR: ", err)
		w.WriteHeader(404)
		w.Write([]byte("Not Found"))
		return
	}

	// format a response object
	res := modelsResponses.ResponseUpdate{
		ID:      updatedID,
		Message: "License updated successfully",
	}

	// send the response
	json.NewEncoder(w).Encode(res)
}

// DeleteLicense will delete a specific license
func DeleteLicense(w http.ResponseWriter, r *http.Request) {

	// get the id from the request params, key is "id"
	params := mux.Vars(r)

	var id int64
	// convert the id type from string to int
	idTemp, err := strconv.Atoi(params["id"])
	id = int64(idTemp)

	if err != nil {
		fmt.Println("ID could not be converted to integer. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("ID could not be converted to integer. ERROR: %v", err)))
		return
	}

	// detete the license form the database
	deletedID, err := handlerModels.DeleteLicense(id)

	if err != nil {
		fmt.Println("Unable to delete. ERROR: ", err)
		w.WriteHeader(404)
		w.Write([]byte("Not Found"))
		return
	}

	// format a response object
	res := modelsResponses.ResponseDelete{
		ID:      deletedID,
		Message: "License deleted successfully",
	}

	// send the response
	json.NewEncoder(w).Encode(res)
}
