package handler

import (
	handlerModels "backend-golang/api/v1/handler/models" // models package where routes 1 schema is defined
	"backend-golang/api/v1/helper"
	"backend-golang/api/v1/models"
	modelsResponses "backend-golang/api/v1/models/responses"
	"encoding/json" // package to encode and decode the json into struct and vice versa
	"errors"
	"fmt"
	"io"
	"log"
	"net/http" // used to access the request and response object of the api
	"os"
	"strconv"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql" // postgres golang driver
	"github.com/gorilla/mux"
)

// GetActiveProject will return all the active projects
func GetActiveProject(w http.ResponseWriter, r *http.Request) {

	size, err := strconv.Atoi(r.URL.Query().Get("size"))

	if err != nil {
		fmt.Println("ID could not be converted to integer. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("ID could not be converted to integer. ERROR: %v", err)))
		return
	}
	// get all the projects in the db
	users, err := handlerModels.GetActiveProjects(size)

	if err != nil {
		log.Fatalf("Unable to get active projects. %v", err)
	}

	// send all the projects as response
	json.NewEncoder(w).Encode(users)
}

// GetArchivedProject will return all the archived projects
func GetArchivedProject(w http.ResponseWriter, r *http.Request) {

	size, err := strconv.Atoi(r.URL.Query().Get("size"))

	if err != nil {
		fmt.Println("ID could not be converted to integer. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("ID could not be converted to integer. ERROR: %v", err)))
		return
	}
	// get all the projects in the db
	users, err := handlerModels.GetArchivedProjects(size)

	if err != nil {
		log.Fatalf("Unable to get archived projects. %v", err)
	}

	// send all the projects as response
	json.NewEncoder(w).Encode(users)
}

// CreateProject will add a project to the database
func CreateProject(w http.ResponseWriter, r *http.Request) {

	var project models.Project

	_, handler, err := r.FormFile("file")
	if err != nil {
		err = errors.New("no file found")
		fmt.Println("Unable to create. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("Unable to create. ERROR: %v", err)))
		return
	}

	fileNameSplit := strings.Split(handler.Filename, ".")
	filenameExt := fileNameSplit[len(fileNameSplit)-1]
	// decode the json request to filters
	r.ParseForm()

	project.Name = r.FormValue("name")
	project.IDProject = r.FormValue("name")
	project.Description = r.FormValue("description")
	project.Annotations = r.FormValue("annotations")
	project.UniqueFile, _ = strconv.Atoi(r.FormValue("uniqueFile"))
	project.Url = fmt.Sprintf("%s_%d.%s", strings.ReplaceAll(r.FormValue("name"), " ", "_"), time.Now().Unix(), filenameExt)

	// if err != nil {
	// 	fmt.Println("Unable to decode the request body. ERROR: ", err)
	// 	w.WriteHeader(400)
	// 	w.Write([]byte(fmt.Sprintf("Unable to decode the request body. ERROR: %v", err)))
	// 	return
	// }

	project.ExcludeEverisLib, _ = strconv.Atoi(r.FormValue("excludeEverisLib"))

	// if err != nil {
	// 	fmt.Println("Unable to decode the request body. ERROR: ", err)
	// 	w.WriteHeader(400)
	// 	w.Write([]byte(fmt.Sprintf("Unable to decode the request body. ERROR: %v", err)))
	// 	return
	// }

	if project.Name == "" {
		err := errors.New("the project name can't be ''")
		fmt.Println("Unable to create. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("Unable to create. ERROR: %v", err)))
		return
	}
	// if project.UniqueFile == 0 {
	// 	err := errors.New("need to check if unique file or not")
	// 	fmt.Println("Unable to create. ERROR: ", err)
	// 	w.WriteHeader(400)
	// 	w.Write([]byte(fmt.Sprintf("Unable to create. ERROR: %v", err)))
	// 	return
	// }

	// add the project to the database
	insertID, err2 := handlerModels.CreateProject(project)

	if err2 != nil {
		fmt.Println("Unable to create. ERROR: ", err2)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("Unable to create. ERROR: %v", err2)))
		return
	}

	w.WriteHeader(201)
	// format a response object
	res := modelsResponses.ResponseCreate{
		ID:      insertID,
		Message: "License created successfully",
	}

	err = uploadFile(w, r, project.Url)

	if err != nil {
		fmt.Println("Unable to create. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("Unable to create. ERROR: %v", err)))
		return
	}
	// Start simulation lauch of the project
	helper.LaunchProject(insertID)

	// send the response
	json.NewEncoder(w).Encode(res)
}

// GetProject will return a specific project
func GetProject(w http.ResponseWriter, r *http.Request) {

	// get the id from the request params, key is "id"
	params := mux.Vars(r)

	// convert the id type from string to int
	id, err := strconv.Atoi(params["id"])

	if err != nil {
		fmt.Println("ID could not be converted to integer. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("ID could not be converted to integer. ERROR: %v", err)))
		return
	}

	// get the project form the database
	project, err2 := handlerModels.GetProject(id)

	if err2 != nil {
		// there was an error
		fmt.Println("Unable to get project. ", err)
		w.WriteHeader(404)
		w.Write([]byte("Not Found"))
		return
	}

	// send the response
	json.NewEncoder(w).Encode(project)
}

// UpdateProject will update a specific project
func UpdateProject(w http.ResponseWriter, r *http.Request) {
	var project models.Project

	_, handler, err := r.FormFile("file")
	if err == nil {
		fileNameSplit := strings.Split(handler.Filename, ".")
		filenameExt := fileNameSplit[len(fileNameSplit)-1]
		project.Url = fmt.Sprintf("%s_%d.%s", strings.ReplaceAll(r.FormValue("name"), " ", "_"), time.Now().Unix(), filenameExt)
	}
	// get the id from the request params, key is "id"
	params := mux.Vars(r)

	var id int64
	// convert the id type from string to int
	idTemp, err := strconv.Atoi(params["id"])
	id = int64(idTemp)

	if err != nil {
		fmt.Println("ID could not be converted to integer. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("ID could not be converted to integer. ERROR: %v", err)))
		return
	}

	project.Name = r.FormValue("name")
	project.IDProject = r.FormValue("name")
	project.Description = r.FormValue("description")
	project.Annotations = r.FormValue("annotations")
	project.UniqueFile, _ = strconv.Atoi(r.FormValue("uniqueFile"))

	// update the project form the database
	updatedID, fileToDelete, err3 := handlerModels.UpdateProject(id, project)

	if err3 != nil {
		fmt.Println("Unable to update. ERROR: ", err3)
		w.WriteHeader(404)
		w.Write([]byte("Not Found"))
		return
	}
	if fileToDelete != "" {
		err = deleteFile(fileToDelete)

		if err != nil {
			fmt.Println("Unable to delete previous file. ERROR: ", err)
			w.WriteHeader(400)
			w.Write([]byte(fmt.Sprintf("Unable to delete previous file. ERROR: %v", err)))
			return
		}

		err = uploadFile(w, r, project.Url)

		if err != nil {
			fmt.Println("Unable to upload file. ERROR: ", err)
			w.WriteHeader(400)
			w.Write([]byte(fmt.Sprintf("Unable to upload file. ERROR: %v", err)))
			return
		}
		// Start simulation lauch of the project
		helper.LaunchProject(updatedID)
	}

	// format a response object
	res := modelsResponses.ResponseUpdate{
		ID:      updatedID,
		Message: "Project updated successfully",
	}

	// send the response
	json.NewEncoder(w).Encode(res)
}

// GetFile will return the file
func GetFile(w http.ResponseWriter, r *http.Request) {
	// get the id from the request params, key is "id"
	params := mux.Vars(r)

	var id int64
	// convert the id type from string to int
	idTemp, err := strconv.Atoi(params["id"])
	id = int64(idTemp)
	if err != nil {
		fmt.Println("ID could not be converted to integer. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("ID could not be converted to integer. ERROR: %v", err)))
		return
	}
	fileToDownload, err := handlerModels.GetProjectFile(id)

	if err != nil {
		fmt.Println("Unable to download. ERROR: ", err)
		w.WriteHeader(404)
		w.Write([]byte("Not Found"))
		return
	}

	err = downloadFile(w, r, fileToDownload)
	// err := downloadFile(w, r, "db.sql")
	if err != nil {
		fmt.Println("Unable to dwonload file. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("Unable to download file. ERROR: %v", err)))
		return
	}
}

// DeleteProject will delete the project
func DeleteProject(w http.ResponseWriter, r *http.Request) {

	// get the id from the request params, key is "id"
	params := mux.Vars(r)

	var id int64
	// convert the id type from string to int
	idTemp, err := strconv.Atoi(params["id"])
	id = int64(idTemp)

	if err != nil {
		fmt.Println("ID could not be converted to integer. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("ID could not be converted to integer. ERROR: %v", err)))
		return
	}

	// detete the license form the database
	deletedID, fileToDelete, err := handlerModels.DeleteProject(id)

	if err != nil {
		fmt.Println("Unable to delete. ERROR: ", err)
		w.WriteHeader(404)
		w.Write([]byte("Not Found"))
		return
	}

	err = deleteFile(fileToDelete)

	if err != nil {
		fmt.Println("Unable to delete previous file. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("Unable to delete previous file. ERROR: %v", err)))
		return
	}

	// format a response object
	res := modelsResponses.ResponseDelete{
		ID:      deletedID,
		Message: "Project deleted successfully",
	}

	// send the response
	json.NewEncoder(w).Encode(res)

}

// RelaunchProject will relauch the project
func RelaunchProject(w http.ResponseWriter, r *http.Request) {

	// get the id from the request params, key is "id"
	params := mux.Vars(r)

	var id int64
	// convert the id type from string to int
	idTemp, err := strconv.Atoi(params["id"])
	id = int64(idTemp)

	if err != nil {
		fmt.Println("ID could not be converted to integer. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("ID could not be converted to integer. ERROR: %v", err)))
		return
	}
	// Start simulation lauch of the project
	helper.LaunchProject(id)
	// format a response object
	res := modelsResponses.ResponseDelete{
		ID:      id,
		Message: "Project relauch successfully",
	}

	// send the response
	json.NewEncoder(w).Encode(res)

}

// ActivateProject will activate a specific project
func ActivateProject(w http.ResponseWriter, r *http.Request) {

	// get the id from the request params, key is "id"
	params := mux.Vars(r)

	var id int64
	// convert the id type from string to int
	idTemp, err := strconv.Atoi(params["id"])
	id = int64(idTemp)

	if err != nil {
		fmt.Println("ID could not be converted to integer. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("ID could not be converted to integer. ERROR: %v", err)))
		return
	}

	// update the project form the database
	updatedID, err3 := handlerModels.ActivateProject(id)

	if err3 != nil {
		fmt.Println("Unable to update. ERROR: ", err3)
		w.WriteHeader(404)
		w.Write([]byte("Not Found"))
		return
	}

	// format a response object
	res := modelsResponses.ResponseUpdate{
		ID:      updatedID,
		Message: "Project activated successfully",
	}

	// send the response
	json.NewEncoder(w).Encode(res)
}

// ArchivateProject will archive a specific project
func ArchivateProject(w http.ResponseWriter, r *http.Request) {

	// get the id from the request params, key is "id"
	params := mux.Vars(r)

	var id int64
	// convert the id type from string to int
	idTemp, err := strconv.Atoi(params["id"])
	id = int64(idTemp)

	if err != nil {
		fmt.Println("ID could not be converted to integer. ERROR: ", err)
		w.WriteHeader(400)
		w.Write([]byte(fmt.Sprintf("ID could not be converted to integer. ERROR: %v", err)))
		return
	}

	// update the project form the database
	updatedID, err3 := handlerModels.ArchivateProject(id)

	if err3 != nil {
		fmt.Println("Unable to update. ERROR: ", err3)
		w.WriteHeader(404)
		w.Write([]byte("Not Found"))
		return
	}

	// format a response object
	res := modelsResponses.ResponseUpdate{
		ID:      updatedID,
		Message: "Project archived successfully",
	}

	// send the response
	json.NewEncoder(w).Encode(res)
}

// uploadFile will add the file to the server
func uploadFile(w http.ResponseWriter, r *http.Request, newFileName string) (err error) {

	// Maximum upload of 10 MB files
	r.ParseMultipartForm(10 << 20)

	// Get handler for filename, size and headers
	file, handler, err := r.FormFile("file")
	if err != nil {
		err = errors.New("no file found")
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return err
	}

	defer file.Close()
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	// Create file
	path := "projectFiles"
	if _, err := os.Stat(path); errors.Is(err, os.ErrNotExist) {
		err := os.Mkdir(path, os.ModePerm)
		if err != nil {
			log.Fatalf("Error creating path. %v", err)
		}
	}
	dst, err := os.Create(path + "/" + newFileName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return err
	}
	defer dst.Close()

	// Copy the uploaded file to the created file on the filesystem
	if _, err := io.Copy(dst, file); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return err
	}

	// fmt.Fprintf(w, "Successfully Uploaded File\n")

	return err
}

// downloadFile will download the file to the server
func downloadFile(w http.ResponseWriter, r *http.Request, fileName string) (err error) {
	path := "projectFiles"

	file, err := os.Open(path + "/" + fileName)

	if err != nil {
		http.Error(w, "File not found.", 404) //return 404 if file is not found
		return err
	}
	defer file.Close() //Close after function return

	tempBuffer := make([]byte, 512)                       //Create a byte array to read the file later
	file.Read(tempBuffer)                                 //Read the file into  byte
	fileContentType := http.DetectContentType(tempBuffer) //Get file header

	fileStat, _ := file.Stat()                         //Get info from file
	fileSize := strconv.FormatInt(fileStat.Size(), 10) //Get file size as a string
	fmt.Println(fileContentType)
	//Set the headers
	w.Header().Set("Content-Disposition", "attachment; filename="+fileName)
	w.Header().Set("Content-Type", fileContentType+";"+fileName)
	w.Header().Set("Content-Length", fileSize)

	file.Seek(0, 0)  //We read 512 bytes from the file already so we reset the offset back to 0
	io.Copy(w, file) //'Copy' the file to the client
	return
}

// deleteFile will delete the file to the server
func deleteFile(fileName string) (err error) {
	path := "projectFiles"

	err = os.Remove(path + "/" + fileName)

	if err != nil {
		return err
	}

	return err
}
